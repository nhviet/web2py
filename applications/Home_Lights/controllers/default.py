# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
# This is a samples controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
# - call exposes all registered services (none by default)
#########################################################################
import os
import GPIOClient as GPIO
import time
import subprocess
import signal

from gpiozero import LED
from time import sleep
from datetime import date
from datetime import datetime
# from bson import json_util
# import json

# import psutil

LIGHTBOARD_SCRIPT = "lightboard_controller.py"
FACE_DETECT_SCRIPT = os.path.dirname(
    os.path.abspath(__file__)) + "/../../../neg/facedetect.py"


@auth.requires_login()
def index():
    lights = db().select(db.lights.ALL, orderby=db.lights.id)
    return dict(lights=lights, board_status=request.vars['board_status'])


@auth.requires_login()
def allon():
    user_in_charge = False
    fukuri_users = db().select(db.fukuri_user.ALL,
                               orderby=db.fukuri_user.DutyDone | db.fukuri_user.UpdatedAt)
    today = datetime.now()
    today_str = today.strftime("%Y/%m/%d")
    if len(fukuri_users) > 0:
        for fukuri_user in fukuri_users:
            # user_led_ids.append(fukuri_user.Name + ":" + fukuri_user.AvailableLightId)
            if fukuri_user.DutyDone and fukuri_user.UpdatedAt and today_str == fukuri_user.UpdatedAt.strftime(
                    "%Y/%m/%d"):
                user_in_charge = fukuri_user
                break

        if not user_in_charge:
            user_in_charge = fukuri_users[0]
            db(db.fukuri_user.FukuriProdUseId == user_in_charge.FukuriProdUseId).update(
                DutyDone=True, UpdatedAt=today)

    print "Today in charge person is ", user_in_charge.Name

    os.system('pulseaudio --start')
    global LIGHTBOARD_SCRIPT
    duty_light = "duty_light_id:" + str(user_in_charge.DutyLightId)
    fukuri_user_str = ';'.join([str({'name': fukuri_user.Name,
                                     'id': fukuri_user.FukuriProdUseId,
                                     'available_led_id': fukuri_user.AvailableLightId}) for fukuri_user in fukuri_users])
    proc = subprocess.Popen(["python", os.path.dirname(os.path.abspath(
        __file__)) + "/" + LIGHTBOARD_SCRIPT, fukuri_user_str, duty_light])

    global FACE_DETECT_SCRIPT
    os.system('sudo modprobe bcm2835-v4l2')
    proc = subprocess.Popen(
        ["python", FACE_DETECT_SCRIPT, fukuri_user_str, duty_light])

    return redirect(URL('index', vars=dict(board_status=1)))


@auth.requires_login()
def alloff():
    global LIGHTBOARD_SCRIPT
    global FACE_DETECT_SCRIPT

    # cmd = 'pkill -f ' + LIGHTBOARD_SCRIPT + ' --signal=' + str(signal.SIGUSR1)
    cmd = 'pkill  -f ' + LIGHTBOARD_SCRIPT
    os.system(cmd)
    print "All off with cmd: ", cmd

    cmd = 'pkill  -f ' + FACE_DETECT_SCRIPT
    os.system(cmd)
    print "All off with cmd: ", cmd
    return redirect(URL('index', vars=dict(board_status=0)))
