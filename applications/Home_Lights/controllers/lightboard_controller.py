#!/usr/bin/env python
# http://stackoverflow.com/questions/151199/how-do-i-calculate-number-of-days-betwen-two-dates-using-python
# python -m py_compile script.py


from gpiozero import LED
from time import sleep
from datetime import date
from datetime import datetime
import signal
import os
import subprocess
import sys
import requests

# Get all fukuri users data from database created at web2py
fukuri_users_str = sys.argv[1]
# print "fukuri_users_str: ", fukuri_users_str
fukuri_users = [eval(user) for user in fukuri_users_str.split(';')]
daily_duty = {k: v for k, v in [sys.argv[2].split(':')]}
# print "daily_duty: ", daily_duty

leds = dict({
    2: LED(2),
    3: LED(3),
    4: LED(4),
    17: LED(17),
    27: LED(27),
    22: LED(22),
    10: LED(10),
    9: LED(9),
    11: LED(11),
    5: LED(5),
    6: LED(6),
    13: LED(13),
    19: LED(19),
    26: LED(26),
    14: LED(14),
    15: LED(15),
    18: LED(18),
    23: LED(23),
    24: LED(24),
    25: LED(25),
    8: LED(8),
    7: LED(7),
    12: LED(12),
    16: LED(16),
    20: LED(20),
    21: LED(21),
})


def turn_off_all_leds():
    for led in leds.values():
        led.off()


def turn_off_raspberry(signum, frame):
    "Callback invoked when a signal is received"
    for led in leds.values():
        led.blink(0.1, 0.1)

    sleep(2)

    turn_off_all_leds()

    sys.exit()


def refresh_checkin_status(signum, frame):
    print " Loading checkin status ....."
    load_checkin_status()


# Waiting for terminal signal from web
signal.signal(signal.SIGTERM, turn_off_raspberry)

# Waiting for checked in successful signal from face recognizing script
signal.signal(signal.SIGHUP, refresh_checkin_status)


def toggle_user_led(user_id, in_ofice):
    for user in fukuri_users:
        if user['id'] == user_id:
            led_id = int(user['available_led_id'])
            led = leds[led_id]
            if not in_ofice:
                led.blink()
                play_leave_office_music(user)
            else:
                led.on()
                play_back_to_office_music(user)
            print "Toogled on led for ", user['name'], " on led pin ", led_id


def welcome_user(user_id):
    for user in fukuri_users:
        if user['id'] == user_id:
            led_id = int(user['available_led_id'])
            led = leds[led_id]
            led.on()
            play_welcome_music(user)
            print "Turned on led for ", user['name'], " on led pin ", led_id


def play_leave_office_music(user):
    espeak_cmd = 'espeak "itterasshai, ' + user['name'] + '-san " 2>/dev/null'
    subprocess.Popen([espeak_cmd], shell=True)


def play_back_to_office_music(user):
    espeak_cmd = 'espeak "okaerinasai, ' + user['name'] + '-san " 2>/dev/null'
    subprocess.Popen([espeak_cmd], shell=True)


def play_welcome_music(user):
    espeak_cmd = 'espeak "Welcome to office, ' + \
        user['name'] + '-san " 2>/dev/null'
    subprocess.Popen([espeak_cmd], shell=True)


class UserCheckin:
    def __init__(self, user_id, start, end):
        self.user_id = user_id
        self.start = start
        self.end = end
        self.in_office = False


user_checked_in_data = dict()

url = "https://tr-fukuri.jp/api/sw/work/users"
today = datetime.now().strftime("%Y/%m/%d")
print "Today is ", today
querystring = {
    "userId": "72",
    "loginUserId": "72",
    "companyId": "3",
    "token": "904c0369f607677e6dab5eb531a675d4",
    "timezone": "Asia/Saigon",
    "deviceType": "A",
    "version": "2.0",
    "serviceCd": "SW",
    "searchDateString": today,
    "language": "en"}

headers = {
    'cache-control': "no-cache",
    # 'postman-token': "ec2c5cc6-1c99-e9df-9d26-50682f13bd08"
}


def load_checkin_status():
    try:
        response = requests.request(
            "GET", url, headers=headers, params=querystring).json()
        work_histories = response['workHistory']
        for work_history in work_histories:
            user = work_history['user']
            if 'startCheckin' in work_history:
                start_checkin = work_history['startCheckin']
            if 'endCheckin' in work_history:
                end_checkin = work_history['endCheckin']

            if start_checkin:
                if (user['key'] in user_checked_in_data):
                    user_checkin = user_checked_in_data[user['key']]
                    if user_checkin.end != end_checkin:
                        user_checkin.in_office = not user_checkin.in_office
                        toggle_user_led(user['key'], user_checkin.in_office)
                        user_checkin.end = end_checkin
                else:
                    user_checkin = UserCheckin(
                        user['key'], start_checkin, end_checkin)
                    user_checkin.in_office = True
                    user_checked_in_data[user['key']] = user_checkin
                    welcome_user(user['key'])
    except requests.exceptions.ConnectionError as err:
        print "Load checkin status failed!!"


##################################################### MAIN #############


# Firstly, turn off all leds
turn_off_all_leds()


# Show LEDs for daily cleaning task
# In charge of cleaning: LEDs on
# Not in charge of cleaning: LEDs off
led_id = int(daily_duty['duty_light_id'])
led_latest_in_charge = leds[led_id]
led_latest_in_charge.on()


# Show LEDs, sounds for user's status:
# Not come office yet: LEDs off
# Come and being at the office: LEDs on
# Come but go out: LEDs blink
while True:
    load_checkin_status()
    sleep(5)
