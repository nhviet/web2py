Main source code files
	applications/Home_Lights/controller/default.py
	applications/Home_Lights/controlleer/lightboard_controller.py
	applications/Home_Lights/views/index.html
	neg/

Flow
	Start button 
		-> default.py 
			-> run child process: light_board_controller.py
			-> run child process: facedetect.py
				-> camera-checkin -> send signal to light_board_controller -> update light

	End button
		-> default.py
			-> end 2 processes above


Environment
	Framework: Web2py
	Installed packages:
		OpenCV3 (Face regconizing)
		espeak, pulseaudio (Text to speech)



GUI:
	One simple page with only one button to turn on/off lightboard

How to run:
	Turn on/off lightboard
		1. cd web2py
		2. python web2py.py
		3. Enter password for administrator (root)
		4. Login to Home_Lights apps with accout viet-nh@trente.asia/root
		5. Turn on/off lightboard
	Add more user
		1. Go to administration page and login
		2. Add new record to database fukuri_user at Home_Lights app
		3. Add new LEDs into board


Issues:
    1. Should change Home_Lights app to LightBoard
	2. Face Regconizing is not good. Sometimes regcognize wrong users
	3. We have to reload the HomeLight index page if we leave it for a while not using.
	4. If in the future we add more users (when new members join Trente), or showing other information on lightboard we have to use extends boards



