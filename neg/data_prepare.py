import numpy as np
import cv2

face_cascade = cv2.CascadeClassifier(
    '/home/pi/opencv-3.1.0/data/haarcascades/haarcascade_frontalface_default.xml')

cap = cv2.VideoCapture(0)

Id = raw_input('enter your id')
sampleNum = 0
while True:
    ret, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)

        sampleNum = sampleNum + 1
        # save the face in datase folder
        cv2.imwrite("dataSet/User." + Id + '.' + str(sampleNum) +
                    ".jpg", gray[y:y + h, x:x + w])

    cv2.imshow('img', img)

    if cv2.waitKey(100) & 0xff == ord('q'):
        break
    elif sampleNum > 10:
        break

cap.release()
cv2.destroyAllWindows()
