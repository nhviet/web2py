import numpy as np
import os
import sys
import cv2
from gpiozero import LED
import signal
import requests
from time import sleep


fukuri_users_str = sys.argv[1]
fukuri_users = [eval(user) for user in fukuri_users_str.split(';')]


def get_user_by_name(user_name):
    if not user_name:
        return False
    for user in fukuri_users:
        if user['name'] == user_name:
            return user


def check_in_for(user):
    global is_checking
    url = "https://tr-fukuri.jp/api/sw/checkin"
    querystring = {
        "userId": user['id'],
        "projectId": "1",
        "location": "Tran Quoc Vuong, Hanoi, Vietnam",
        "gpsLongtitude": "105.7837182",
        "gpsLatitude": "21.0342553",
        "loginUserId": "72",
        "companyId": "3",
        "language": "en",
        "timezone": "Asia/Saigon",
        "deviceType": "A",
        "version": "2.0",
        "serviceCd": "SW",
        "token": "904c0369f607677e6dabeb531a"}

    headers = {
        'cache-control': "no-cache",
        'postman-token': "692c7d08-4d7d-444e-207b-d32a1b396b21"
    }

    try:
        response = requests.request(
            "POST", url, headers=headers, params=querystring).json()
    except requests.exceptions.ConnectionError as err:
        print "Check in failed for ", user.name

    print "Checkin respone: ", response

    if response['status'] == "OK":
        cmd = 'pkill -f lightboard_controller.py --signal=' + \
            str(signal.SIGHUP)
        os.system(cmd)
    else:
        print "Check in failed for ", user.name
    sleep(5)



# START CAPTURING ###############################33
cam = cv2.VideoCapture(0)
detector = cv2.CascadeClassifier(
    '/home/pi/opencv-3.1.0/data/haarcascades/haarcascade_frontalface_default.xml')
rec = cv2.face.createLBPHFaceRecognizer()

trainner_file_path = os.path.dirname(
    os.path.abspath(__file__)) + "/trainner/trainner.yml"
rec.load(trainner_file_path)
Id = 0

font = cv2.FONT_HERSHEY_SIMPLEX
while(True):
    ret, img = cam.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = detector.detectMultiScale(gray, 1.3, 5)
    for (x, y, w, h) in faces:
        user_name = ""
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 4)
        Id = rec.predict(gray[y:y + h, x:x + w])
        if(Id == 1):
            user_name = "Doan HQ"
        elif(Id == 2):
            user_name = "Viet NH"
        elif(Id == 3):
            user_name = "Tanaka Takayuki"
        elif(Id == 4):
            user_name = "Thuy TT"
        elif(Id == 5):
            user_name = "Phuong LT"
        cv2.putText(img, str(user_name), (x, y + h), font,
                    0.5, (11, 255, 255), 2, cv2.LINE_AA)
        user = get_user_by_name(user_name)
        if user:
            check_in_for(user)
            cam = cv2.VideoCapture(0)
            Id = 0
            break
        else:
            print "can not find user $user_name"
    cv2.imshow("Face", img)
    if cv2.waitKey(1) & 0xff == ord('q'):
        break

cam.release()
cv2.destroyAllWindows()
