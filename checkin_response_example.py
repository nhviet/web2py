pulseaudio --start
pgrep -f web2py
kill xxx
{
  "projects": [],
  "checkin": {
    "useNowTime": false,
    "hasWarning": false,
    "infoMap": {}
  },
  "checkInDateTime": "",
  "checkins": [],
  "notifications": [],
  "workingUsers": [],
  "workHistory": [
    {
      "userId": 70,
      "user": {
        "userAccount": "trung-nd",
        "userName": "Trung ND",
        "userNameKana": "Trung ND",
        "userMail": "trung-nd@trente.asia",
        "userType": "N",
        "adminFlag": false,
        "avatarHash": "U-RTuiVvimqu",
        "avatarPath": "/static/account/U-RTuiVvimqu?token=e680cc971f8217071d89825c8d58a76d",
        "dept": {
          "members": [],
          "managers": [],
          "cropping": {},
          "infoMap": {}
        },
        "password": "f5ed8b1a702380cf6feeb94aa7878596ffe612bb71fddd7ad1c0064621a6f8cd82d4c43aceed522b545c1061df407e04bea37ff2769f656e662618c679d4e47a",
        "passcode": "3469",
        "passcodePath": "/static/passcode/3/70?token=e680cc971f8217071d89825c8d58a76d",
        "dateBirth": "1987/10/30 00:00:00",
        "joinDate": "2014/07/31 00:00:00",
        "isActivateSignin": true,
        "isActivatePassword": true,
        "services": [
          {
            "serviceCd": "TC",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_tc.png"
          },
          {
            "serviceCd": "DR",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_dr.png"
          },
          {
            "serviceCd": "MS",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_ms.png"
          },
          {
            "serviceCd": "SW",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_sw.png",
            "lastSignin": "2017/03/01 17:21:33"
          }
        ],
        "lastSignin": "2017/03/27 11:16:32",
        "vcDelegate": {
          "logger": {
            "name": "services.service.company.vc.CompanyUserModelViewController"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 70,
        "token": "e64ad975c7e5004b17551c523074986e",
        "companyId": 3
      },
      "workDate": "2017/04/04 00:00:00",
      "project": {
        "projectName": "TRENTE Vietnam",
        "projectLocation": "Hanoi Office",
        "gpsUse": false,
        "gpsMode": "OFF",
        "avatarHash": "P-xPJoWVyBlJ",
        "avatarPath": "/static/account/P-xPJoWVyBlJ?token=e680cc971f8217071d89825c8d58a76d",
        "timeUnit": "30",
        "users": [
          {
            "workingType": "PP",
            "userId": 140,
            "infoMap": {},
            "key": 242
          },
          {
            "workingType": "PP",
            "userId": 118,
            "infoMap": {},
            "key": 241
          },
          {
            "workingType": "PP",
            "userId": 114,
            "infoMap": {},
            "key": 240
          },
          {
            "workingType": "PP",
            "userId": 72,
            "infoMap": {},
            "key": 239
          },
          {
            "workingType": "PP",
            "userId": 71,
            "infoMap": {},
            "key": 238
          },
          {
            "workingType": "PP",
            "userId": 70,
            "infoMap": {},
            "key": 237
          },
          {
            "workingType": "PP",
            "userId": 69,
            "infoMap": {},
            "key": 236
          },
          {
            "workingType": "PP",
            "userId": 68,
            "infoMap": {},
            "key": 235
          },
          {
            "workingType": "PP",
            "userId": 67,
            "infoMap": {},
            "key": 234
          },
          {
            "workingType": "PP",
            "userId": 66,
            "infoMap": {},
            "key": 233
          },
          {
            "workingType": "PP",
            "userId": 65,
            "infoMap": {},
            "key": 232
          },
          {
            "workingType": "PP",
            "userId": 62,
            "infoMap": {},
            "key": 231
          },
          {
            "workingType": "PP",
            "userId": 61,
            "infoMap": {},
            "key": 230
          },
          {
            "workingType": "PP",
            "userId": 59,
            "infoMap": {},
            "key": 229
          },
          {
            "workingType": "PP",
            "userId": 108,
            "infoMap": {},
            "key": 228
          },
          {
            "workingType": "PP",
            "userId": 116,
            "infoMap": {},
            "key": 227
          },
          {
            "workingType": "PP",
            "userId": 121,
            "infoMap": {},
            "key": 226
          },
          {
            "workingType": "PP",
            "userId": 166,
            "infoMap": {},
            "key": 243
          }
        ],
        "breaks": [
          {
            "startTime": "12:00",
            "endTime": "13:00",
            "infoMap": {},
            "key": 61
          }
        ],
        "worktimeStart": "08:30",
        "worktimeEnd": "17:30",
        "worktimeNightStart": "22:00",
        "worktimeNightEnd": "05:00",
        "vcDelegate": {
          "logger": {
            "name": "services.service.shiftworking.vc.ProjectModelViewControl"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 1,
        "companyId": 3
      },
      "startShift": "08:30",
      "endShift": "17:30",
      "startCheckin": "07:52",
      "endCheckin": "",
      "startWorking": "08:30",
      "endWorking": "17:30",
      "totalWorkTime": "08:00",
      "totalBreakTime": "01:00",
      "totalOverTime": "",
      "totalShortTime": "",
      "totalHolidayTime": "",
      "workStrategy": {},
      "vcDelegate": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "vc": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {}
    },
    {
      "userId": 59,
      "user": {
        "userAccount": "takano-yasuhiro",
        "userName": "高野　康弘",
        "userNameKana": "Yasuhiro Takano",
        "userMail": "takano-yasuhiro@trente.asia",
        "userType": "N",
        "adminFlag": true,
        "avatarHash": "U-uYtzsjyqrc",
        "avatarPath": "/static/account/U-uYtzsjyqrc?token=e680cc971f8217071d89825c8d58a76d",
        "dept": {
          "members": [],
          "managers": [],
          "cropping": {},
          "infoMap": {}
        },
        "password": "73dc24c642c00524eaf318ba1d3b85c22863aab3b070f97d075a0f5509d833e4f8fb9e8793791b35b8bea704a9861957490c8f054428f4648203b9523815294b",
        "passcode": "4094",
        "passcodePath": "/static/passcode/3/59?token=e680cc971f8217071d89825c8d58a76d",
        "dateBirth": "1977/12/25 00:00:00",
        "joinDate": "2014/07/31 00:00:00",
        "isActivateSignin": true,
        "isActivatePassword": true,
        "services": [
          {
            "serviceCd": "TC",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_tc.png",
            "lastSignin": "2017/03/04 10:34:49"
          },
          {
            "serviceCd": "DR",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_dr.png",
            "lastSignin": "2016/12/06 15:36:48"
          },
          {
            "serviceCd": "MS",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_ms.png",
            "lastSignin": "2016/12/07 17:17:02"
          },
          {
            "serviceCd": "SW",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_sw.png",
            "lastSignin": "2017/04/04 17:22:09"
          },
          {
            "serviceCd": "CL",
            "pushSetting": true,
            "lastSignin": "2017/04/04 14:57:32"
          }
        ],
        "lastSignin": "2017/04/04 17:22:09",
        "vcDelegate": {
          "logger": {
            "name": "services.service.company.vc.CompanyUserModelViewController"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 59,
        "token": "d82a3e01fb9babea4d627f3b04194665",
        "companyId": 3
      },
      "workDate": "2017/04/04 00:00:00",
      "project": {
        "projectName": "TRENTE Vietnam",
        "projectLocation": "Hanoi Office",
        "gpsUse": false,
        "gpsMode": "OFF",
        "avatarHash": "P-xPJoWVyBlJ",
        "avatarPath": "/static/account/P-xPJoWVyBlJ?token=e680cc971f8217071d89825c8d58a76d",
        "timeUnit": "30",
        "users": [
          {
            "workingType": "PP",
            "userId": 140,
            "infoMap": {},
            "key": 242
          },
          {
            "workingType": "PP",
            "userId": 118,
            "infoMap": {},
            "key": 241
          },
          {
            "workingType": "PP",
            "userId": 114,
            "infoMap": {},
            "key": 240
          },
          {
            "workingType": "PP",
            "userId": 72,
            "infoMap": {},
            "key": 239
          },
          {
            "workingType": "PP",
            "userId": 71,
            "infoMap": {},
            "key": 238
          },
          {
            "workingType": "PP",
            "userId": 70,
            "infoMap": {},
            "key": 237
          },
          {
            "workingType": "PP",
            "userId": 69,
            "infoMap": {},
            "key": 236
          },
          {
            "workingType": "PP",
            "userId": 68,
            "infoMap": {},
            "key": 235
          },
          {
            "workingType": "PP",
            "userId": 67,
            "infoMap": {},
            "key": 234
          },
          {
            "workingType": "PP",
            "userId": 66,
            "infoMap": {},
            "key": 233
          },
          {
            "workingType": "PP",
            "userId": 65,
            "infoMap": {},
            "key": 232
          },
          {
            "workingType": "PP",
            "userId": 62,
            "infoMap": {},
            "key": 231
          },
          {
            "workingType": "PP",
            "userId": 61,
            "infoMap": {},
            "key": 230
          },
          {
            "workingType": "PP",
            "userId": 59,
            "infoMap": {},
            "key": 229
          },
          {
            "workingType": "PP",
            "userId": 108,
            "infoMap": {},
            "key": 228
          },
          {
            "workingType": "PP",
            "userId": 116,
            "infoMap": {},
            "key": 227
          },
          {
            "workingType": "PP",
            "userId": 121,
            "infoMap": {},
            "key": 226
          },
          {
            "workingType": "PP",
            "userId": 166,
            "infoMap": {},
            "key": 243
          }
        ],
        "breaks": [
          {
            "startTime": "12:00",
            "endTime": "13:00",
            "infoMap": {},
            "key": 61
          }
        ],
        "worktimeStart": "08:30",
        "worktimeEnd": "17:30",
        "worktimeNightStart": "22:00",
        "worktimeNightEnd": "05:00",
        "vcDelegate": {
          "logger": {
            "name": "services.service.shiftworking.vc.ProjectModelViewControl"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 1,
        "companyId": 3
      },
      "startShift": "08:30",
      "endShift": "17:30",
      "startCheckin": "07:59",
      "endCheckin": "",
      "startWorking": "08:30",
      "endWorking": "17:30",
      "totalWorkTime": "08:00",
      "totalBreakTime": "01:00",
      "totalOverTime": "",
      "totalShortTime": "",
      "totalHolidayTime": "",
      "workStrategy": {},
      "vcDelegate": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "vc": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {}
    },
    {
      "userId": 140,
      "user": {
        "userAccount": "tanaka-takayuki",
        "userName": "Tanaka Takayuki",
        "userNameKana": "Tanaka Takayuki",
        "userMail": "tanaka-takayuki@trente.asia",
        "userType": "N",
        "adminFlag": true,
        "avatarHash": "U-aTVRmxruXm",
        "avatarPath": "/static/account/U-aTVRmxruXm?token=e680cc971f8217071d89825c8d58a76d",
        "dept": {
          "members": [],
          "managers": [],
          "cropping": {},
          "infoMap": {}
        },
        "password": "7ba3365acaafbb26f2562fad8f54687084e2b734c672ef690a1a6030d04eb10594e98e952cd0f38b4c1e34dadb8463ee49077d1b53e2245fa3268ee72c4ca209",
        "passcode": "0096",
        "passcodePath": "/static/passcode/3/140?token=e680cc971f8217071d89825c8d58a76d",
        "dateBirth": "1978/12/17 00:00:00",
        "joinDate": "2017/03/01 00:00:00",
        "isActivateSignin": true,
        "isActivatePassword": true,
        "services": [
          {
            "serviceCd": "TC",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_tc.png"
          },
          {
            "serviceCd": "DR",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_dr.png"
          },
          {
            "serviceCd": "MS",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_ms.png"
          },
          {
            "serviceCd": "SW",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_sw.png"
          }
        ],
        "lastSignin": "2017/04/04 10:38:25",
        "vcDelegate": {
          "logger": {
            "name": "services.service.company.vc.CompanyUserModelViewController"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 140,
        "token": "7fa6d4fb0b4fffdb567edcb80a1b57a1",
        "companyId": 3
      },
      "workDate": "2017/04/04 00:00:00",
      "project": {
        "projectName": "TRENTE Vietnam",
        "projectLocation": "Hanoi Office",
        "gpsUse": false,
        "gpsMode": "OFF",
        "avatarHash": "P-xPJoWVyBlJ",
        "avatarPath": "/static/account/P-xPJoWVyBlJ?token=e680cc971f8217071d89825c8d58a76d",
        "timeUnit": "30",
        "users": [
          {
            "workingType": "PP",
            "userId": 140,
            "infoMap": {},
            "key": 242
          },
          {
            "workingType": "PP",
            "userId": 118,
            "infoMap": {},
            "key": 241
          },
          {
            "workingType": "PP",
            "userId": 114,
            "infoMap": {},
            "key": 240
          },
          {
            "workingType": "PP",
            "userId": 72,
            "infoMap": {},
            "key": 239
          },
          {
            "workingType": "PP",
            "userId": 71,
            "infoMap": {},
            "key": 238
          },
          {
            "workingType": "PP",
            "userId": 70,
            "infoMap": {},
            "key": 237
          },
          {
            "workingType": "PP",
            "userId": 69,
            "infoMap": {},
            "key": 236
          },
          {
            "workingType": "PP",
            "userId": 68,
            "infoMap": {},
            "key": 235
          },
          {
            "workingType": "PP",
            "userId": 67,
            "infoMap": {},
            "key": 234
          },
          {
            "workingType": "PP",
            "userId": 66,
            "infoMap": {},
            "key": 233
          },
          {
            "workingType": "PP",
            "userId": 65,
            "infoMap": {},
            "key": 232
          },
          {
            "workingType": "PP",
            "userId": 62,
            "infoMap": {},
            "key": 231
          },
          {
            "workingType": "PP",
            "userId": 61,
            "infoMap": {},
            "key": 230
          },
          {
            "workingType": "PP",
            "userId": 59,
            "infoMap": {},
            "key": 229
          },
          {
            "workingType": "PP",
            "userId": 108,
            "infoMap": {},
            "key": 228
          },
          {
            "workingType": "PP",
            "userId": 116,
            "infoMap": {},
            "key": 227
          },
          {
            "workingType": "PP",
            "userId": 121,
            "infoMap": {},
            "key": 226
          },
          {
            "workingType": "PP",
            "userId": 166,
            "infoMap": {},
            "key": 243
          }
        ],
        "breaks": [
          {
            "startTime": "12:00",
            "endTime": "13:00",
            "infoMap": {},
            "key": 61
          }
        ],
        "worktimeStart": "08:30",
        "worktimeEnd": "17:30",
        "worktimeNightStart": "22:00",
        "worktimeNightEnd": "05:00",
        "vcDelegate": {
          "logger": {
            "name": "services.service.shiftworking.vc.ProjectModelViewControl"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 1,
        "companyId": 3
      },
      "startShift": "08:30",
      "endShift": "17:30",
      "startCheckin": "08:02",
      "endCheckin": "",
      "startWorking": "08:30",
      "endWorking": "17:30",
      "totalWorkTime": "08:00",
      "totalBreakTime": "01:00",
      "totalOverTime": "",
      "totalShortTime": "",
      "totalHolidayTime": "",
      "workStrategy": {},
      "vcDelegate": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "vc": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {}
    },
    {
      "userId": 72,
      "user": {
        "userAccount": "viet-nh",
        "userName": "Viet NH",
        "userNameKana": "Viet NH",
        "userMail": "viet-nh@trente.asia",
        "userType": "N",
        "adminFlag": false,
        "avatarHash": "U-TBxIbKXubL",
        "avatarPath": "/static/account/U-TBxIbKXubL?token=e680cc971f8217071d89825c8d58a76d",
        "dept": {
          "members": [],
          "managers": [],
          "cropping": {},
          "infoMap": {}
        },
        "password": "2935abac79b93fd5f76ad791834f6af71572daa2d68d8ea43c472e7525540b889896a159f030b49e0d2321d7c07b282a6df7f9c3dd4850bf26b8779cbde4b40c",
        "passcode": "4321",
        "passcodePath": "/static/passcode/3/72?token=e680cc971f8217071d89825c8d58a76d",
        "dateBirth": "1990/03/12 00:00:00",
        "joinDate": "2015/06/22 00:00:00",
        "isActivateSignin": true,
        "isActivatePassword": true,
        "services": [
          {
            "serviceCd": "TC",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_tc.png"
          },
          {
            "serviceCd": "DR",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_dr.png",
            "lastSignin": "2017/04/04 15:51:56"
          },
          {
            "serviceCd": "MS",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_ms.png"
          },
          {
            "serviceCd": "SW",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_sw.png",
            "lastSignin": "2017/04/04 17:22:10"
          }
        ],
        "lastSignin": "2017/04/04 17:22:10",
        "vcDelegate": {
          "logger": {
            "name": "services.service.company.vc.CompanyUserModelViewController"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 72,
        "token": "904c0369f607677e6dab5eb531a675d4",
        "companyId": 3
      },
      "workDate": "2017/04/04 00:00:00",
      "project": {
        "projectName": "TRENTE Vietnam",
        "projectLocation": "Hanoi Office",
        "gpsUse": false,
        "gpsMode": "OFF",
        "avatarHash": "P-xPJoWVyBlJ",
        "avatarPath": "/static/account/P-xPJoWVyBlJ?token=e680cc971f8217071d89825c8d58a76d",
        "timeUnit": "30",
        "users": [
          {
            "workingType": "PP",
            "userId": 140,
            "infoMap": {},
            "key": 242
          },
          {
            "workingType": "PP",
            "userId": 118,
            "infoMap": {},
            "key": 241
          },
          {
            "workingType": "PP",
            "userId": 114,
            "infoMap": {},
            "key": 240
          },
          {
            "workingType": "PP",
            "userId": 72,
            "infoMap": {},
            "key": 239
          },
          {
            "workingType": "PP",
            "userId": 71,
            "infoMap": {},
            "key": 238
          },
          {
            "workingType": "PP",
            "userId": 70,
            "infoMap": {},
            "key": 237
          },
          {
            "workingType": "PP",
            "userId": 69,
            "infoMap": {},
            "key": 236
          },
          {
            "workingType": "PP",
            "userId": 68,
            "infoMap": {},
            "key": 235
          },
          {
            "workingType": "PP",
            "userId": 67,
            "infoMap": {},
            "key": 234
          },
          {
            "workingType": "PP",
            "userId": 66,
            "infoMap": {},
            "key": 233
          },
          {
            "workingType": "PP",
            "userId": 65,
            "infoMap": {},
            "key": 232
          },
          {
            "workingType": "PP",
            "userId": 62,
            "infoMap": {},
            "key": 231
          },
          {
            "workingType": "PP",
            "userId": 61,
            "infoMap": {},
            "key": 230
          },
          {
            "workingType": "PP",
            "userId": 59,
            "infoMap": {},
            "key": 229
          },
          {
            "workingType": "PP",
            "userId": 108,
            "infoMap": {},
            "key": 228
          },
          {
            "workingType": "PP",
            "userId": 116,
            "infoMap": {},
            "key": 227
          },
          {
            "workingType": "PP",
            "userId": 121,
            "infoMap": {},
            "key": 226
          },
          {
            "workingType": "PP",
            "userId": 166,
            "infoMap": {},
            "key": 243
          }
        ],
        "breaks": [
          {
            "startTime": "12:00",
            "endTime": "13:00",
            "infoMap": {},
            "key": 61
          }
        ],
        "worktimeStart": "08:30",
        "worktimeEnd": "17:30",
        "worktimeNightStart": "22:00",
        "worktimeNightEnd": "05:00",
        "vcDelegate": {
          "logger": {
            "name": "services.service.shiftworking.vc.ProjectModelViewControl"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 1,
        "companyId": 3
      },
      "startShift": "08:30",
      "endShift": "17:30",
      "startCheckin": "08:12",
      "endCheckin": "16:40",
      "startWorking": "08:30",
      "endWorking": "17:30",
      "totalWorkTime": "08:00",
      "totalBreakTime": "01:00",
      "totalOverTime": "",
      "totalShortTime": "",
      "totalHolidayTime": "",
      "workStrategy": {},
      "vcDelegate": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "vc": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {}
    },
    {
      "userId": 66,
      "user": {
        "userAccount": "phuong-lt",
        "userName": "Phuong LT",
        "userNameKana": "Phuong LT",
        "userMail": "phuong-lt@trente.asia",
        "userType": "N",
        "adminFlag": false,
        "avatarHash": "U-ZoaxsKsohM",
        "avatarPath": "/static/account/U-ZoaxsKsohM?token=e680cc971f8217071d89825c8d58a76d",
        "dept": {
          "members": [],
          "managers": [],
          "cropping": {},
          "infoMap": {}
        },
        "password": "04a73c9ae9fd71f003d4a2928da170223711b916e32369c995af9629cdf340eaf1b198d71faff93165fa40d374711ee44685bc8eb31e2cb0eb162f560c9ac11b",
        "passcode": "2689",
        "passcodePath": "/static/passcode/3/66?token=e680cc971f8217071d89825c8d58a76d",
        "dateBirth": "1992/10/19 00:00:00",
        "joinDate": "2015/06/01 00:00:00",
        "isActivateSignin": true,
        "isActivatePassword": true,
        "services": [
          {
            "serviceCd": "TC",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_tc.png"
          },
          {
            "serviceCd": "DR",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_dr.png"
          },
          {
            "serviceCd": "MS",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_ms.png"
          },
          {
            "serviceCd": "SW",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_sw.png",
            "lastSignin": "2017/02/27 08:23:37"
          }
        ],
        "lastSignin": "2017/03/23 11:25:56",
        "vcDelegate": {
          "logger": {
            "name": "services.service.company.vc.CompanyUserModelViewController"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 66,
        "token": "3df51d19016769e159415ade99982d50",
        "companyId": 3
      },
      "workDate": "2017/04/04 00:00:00",
      "project": {
        "projectName": "TRENTE Vietnam",
        "projectLocation": "Hanoi Office",
        "gpsUse": false,
        "gpsMode": "OFF",
        "avatarHash": "P-xPJoWVyBlJ",
        "avatarPath": "/static/account/P-xPJoWVyBlJ?token=e680cc971f8217071d89825c8d58a76d",
        "timeUnit": "30",
        "users": [
          {
            "workingType": "PP",
            "userId": 140,
            "infoMap": {},
            "key": 242
          },
          {
            "workingType": "PP",
            "userId": 118,
            "infoMap": {},
            "key": 241
          },
          {
            "workingType": "PP",
            "userId": 114,
            "infoMap": {},
            "key": 240
          },
          {
            "workingType": "PP",
            "userId": 72,
            "infoMap": {},
            "key": 239
          },
          {
            "workingType": "PP",
            "userId": 71,
            "infoMap": {},
            "key": 238
          },
          {
            "workingType": "PP",
            "userId": 70,
            "infoMap": {},
            "key": 237
          },
          {
            "workingType": "PP",
            "userId": 69,
            "infoMap": {},
            "key": 236
          },
          {
            "workingType": "PP",
            "userId": 68,
            "infoMap": {},
            "key": 235
          },
          {
            "workingType": "PP",
            "userId": 67,
            "infoMap": {},
            "key": 234
          },
          {
            "workingType": "PP",
            "userId": 66,
            "infoMap": {},
            "key": 233
          },
          {
            "workingType": "PP",
            "userId": 65,
            "infoMap": {},
            "key": 232
          },
          {
            "workingType": "PP",
            "userId": 62,
            "infoMap": {},
            "key": 231
          },
          {
            "workingType": "PP",
            "userId": 61,
            "infoMap": {},
            "key": 230
          },
          {
            "workingType": "PP",
            "userId": 59,
            "infoMap": {},
            "key": 229
          },
          {
            "workingType": "PP",
            "userId": 108,
            "infoMap": {},
            "key": 228
          },
          {
            "workingType": "PP",
            "userId": 116,
            "infoMap": {},
            "key": 227
          },
          {
            "workingType": "PP",
            "userId": 121,
            "infoMap": {},
            "key": 226
          },
          {
            "workingType": "PP",
            "userId": 166,
            "infoMap": {},
            "key": 243
          }
        ],
        "breaks": [
          {
            "startTime": "12:00",
            "endTime": "13:00",
            "infoMap": {},
            "key": 61
          }
        ],
        "worktimeStart": "08:30",
        "worktimeEnd": "17:30",
        "worktimeNightStart": "22:00",
        "worktimeNightEnd": "05:00",
        "vcDelegate": {
          "logger": {
            "name": "services.service.shiftworking.vc.ProjectModelViewControl"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 1,
        "companyId": 3
      },
      "startShift": "08:30",
      "endShift": "17:30",
      "startCheckin": "08:13",
      "endCheckin": "",
      "startWorking": "08:30",
      "endWorking": "17:30",
      "totalWorkTime": "08:00",
      "totalBreakTime": "01:00",
      "totalOverTime": "",
      "totalShortTime": "",
      "totalHolidayTime": "",
      "workStrategy": {},
      "vcDelegate": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "vc": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {}
    },
    {
      "userId": 69,
      "user": {
        "userAccount": "thuy-tt",
        "userName": "Thuy TT",
        "userNameKana": "Thuy TT",
        "userMail": "thuy-tt@trente.asia",
        "userType": "N",
        "adminFlag": false,
        "avatarHash": "U-jYEBAZxKFM",
        "avatarPath": "/static/account/U-jYEBAZxKFM?token=e680cc971f8217071d89825c8d58a76d",
        "dept": {
          "members": [],
          "managers": [],
          "cropping": {},
          "infoMap": {}
        },
        "password": "4eb9f875602f5245b12f18d7b6ba47d6f83fca17119c16d7e3e4106495205452b28c71f25e377f236a42779bc0a231a021096b7059726595decd8c3a9675df2a",
        "passcode": "9631",
        "passcodePath": "/static/passcode/3/69?token=e680cc971f8217071d89825c8d58a76d",
        "dateBirth": "1992/09/02 00:00:00",
        "joinDate": "2015/06/01 00:00:00",
        "isActivateSignin": true,
        "isActivatePassword": true,
        "services": [
          {
            "serviceCd": "TC",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_tc.png"
          },
          {
            "serviceCd": "DR",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_dr.png"
          },
          {
            "serviceCd": "MS",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_ms.png"
          },
          {
            "serviceCd": "SW",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_sw.png",
            "lastSignin": "2017/04/03 19:19:29"
          }
        ],
        "lastSignin": "2017/04/04 09:56:22",
        "vcDelegate": {
          "logger": {
            "name": "services.service.company.vc.CompanyUserModelViewController"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 69,
        "token": "1e4ab99108ebaee9cfbaa20d091da57b",
        "companyId": 3
      },
      "workDate": "2017/04/04 00:00:00",
      "project": {
        "projectName": "TRENTE Vietnam",
        "projectLocation": "Hanoi Office",
        "gpsUse": false,
        "gpsMode": "OFF",
        "avatarHash": "P-xPJoWVyBlJ",
        "avatarPath": "/static/account/P-xPJoWVyBlJ?token=e680cc971f8217071d89825c8d58a76d",
        "timeUnit": "30",
        "users": [
          {
            "workingType": "PP",
            "userId": 140,
            "infoMap": {},
            "key": 242
          },
          {
            "workingType": "PP",
            "userId": 118,
            "infoMap": {},
            "key": 241
          },
          {
            "workingType": "PP",
            "userId": 114,
            "infoMap": {},
            "key": 240
          },
          {
            "workingType": "PP",
            "userId": 72,
            "infoMap": {},
            "key": 239
          },
          {
            "workingType": "PP",
            "userId": 71,
            "infoMap": {},
            "key": 238
          },
          {
            "workingType": "PP",
            "userId": 70,
            "infoMap": {},
            "key": 237
          },
          {
            "workingType": "PP",
            "userId": 69,
            "infoMap": {},
            "key": 236
          },
          {
            "workingType": "PP",
            "userId": 68,
            "infoMap": {},
            "key": 235
          },
          {
            "workingType": "PP",
            "userId": 67,
            "infoMap": {},
            "key": 234
          },
          {
            "workingType": "PP",
            "userId": 66,
            "infoMap": {},
            "key": 233
          },
          {
            "workingType": "PP",
            "userId": 65,
            "infoMap": {},
            "key": 232
          },
          {
            "workingType": "PP",
            "userId": 62,
            "infoMap": {},
            "key": 231
          },
          {
            "workingType": "PP",
            "userId": 61,
            "infoMap": {},
            "key": 230
          },
          {
            "workingType": "PP",
            "userId": 59,
            "infoMap": {},
            "key": 229
          },
          {
            "workingType": "PP",
            "userId": 108,
            "infoMap": {},
            "key": 228
          },
          {
            "workingType": "PP",
            "userId": 116,
            "infoMap": {},
            "key": 227
          },
          {
            "workingType": "PP",
            "userId": 121,
            "infoMap": {},
            "key": 226
          },
          {
            "workingType": "PP",
            "userId": 166,
            "infoMap": {},
            "key": 243
          }
        ],
        "breaks": [
          {
            "startTime": "12:00",
            "endTime": "13:00",
            "infoMap": {},
            "key": 61
          }
        ],
        "worktimeStart": "08:30",
        "worktimeEnd": "17:30",
        "worktimeNightStart": "22:00",
        "worktimeNightEnd": "05:00",
        "vcDelegate": {
          "logger": {
            "name": "services.service.shiftworking.vc.ProjectModelViewControl"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 1,
        "companyId": 3
      },
      "startShift": "08:30",
      "endShift": "17:30",
      "startCheckin": "08:15",
      "endCheckin": "",
      "startWorking": "08:30",
      "endWorking": "17:30",
      "totalWorkTime": "08:00",
      "totalBreakTime": "01:00",
      "totalOverTime": "",
      "totalShortTime": "",
      "totalHolidayTime": "",
      "workStrategy": {},
      "vcDelegate": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "vc": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {}
    },
    {
      "userId": 116,
      "user": {
        "userAccount": "hoan-ht",
        "userName": "Hoan HT",
        "userNameKana": "Hoan HT",
        "userMail": "hoan-ht@trente.asia",
        "userType": "N",
        "adminFlag": false,
        "avatarHash": "U-XGiRpWNWCA",
        "avatarPath": "/static/account/U-XGiRpWNWCA?token=e680cc971f8217071d89825c8d58a76d",
        "dept": {
          "members": [],
          "managers": [],
          "cropping": {},
          "infoMap": {}
        },
        "password": "3ec32fb8220d7aa1bc192a5b8cdc198834548e1893e36526b0f2eccac73a94bff357fc8efc24d4bc555fbaa88da684b53a9b5e9b17828bee38744027066ca154",
        "passcode": "3831",
        "passcodePath": "/static/passcode/3/116?token=e680cc971f8217071d89825c8d58a76d",
        "dateBirth": "1994/07/20 00:00:00",
        "joinDate": "2017/01/16 00:00:00",
        "isActivateSignin": true,
        "isActivatePassword": true,
        "services": [
          {
            "serviceCd": "TC",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_tc.png"
          },
          {
            "serviceCd": "DR",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_dr.png"
          },
          {
            "serviceCd": "MS",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_ms.png"
          },
          {
            "serviceCd": "SW",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_sw.png",
            "lastSignin": "2017/03/25 21:55:23"
          }
        ],
        "lastSignin": "2017/04/04 09:37:47",
        "vcDelegate": {
          "logger": {
            "name": "services.service.company.vc.CompanyUserModelViewController"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 116,
        "token": "eb46d55bd03003d68895dc90976f35be",
        "companyId": 3
      },
      "workDate": "2017/04/04 00:00:00",
      "project": {
        "projectName": "TRENTE Vietnam",
        "projectLocation": "Hanoi Office",
        "gpsUse": false,
        "gpsMode": "OFF",
        "avatarHash": "P-xPJoWVyBlJ",
        "avatarPath": "/static/account/P-xPJoWVyBlJ?token=e680cc971f8217071d89825c8d58a76d",
        "timeUnit": "30",
        "users": [
          {
            "workingType": "PP",
            "userId": 140,
            "infoMap": {},
            "key": 242
          },
          {
            "workingType": "PP",
            "userId": 118,
            "infoMap": {},
            "key": 241
          },
          {
            "workingType": "PP",
            "userId": 114,
            "infoMap": {},
            "key": 240
          },
          {
            "workingType": "PP",
            "userId": 72,
            "infoMap": {},
            "key": 239
          },
          {
            "workingType": "PP",
            "userId": 71,
            "infoMap": {},
            "key": 238
          },
          {
            "workingType": "PP",
            "userId": 70,
            "infoMap": {},
            "key": 237
          },
          {
            "workingType": "PP",
            "userId": 69,
            "infoMap": {},
            "key": 236
          },
          {
            "workingType": "PP",
            "userId": 68,
            "infoMap": {},
            "key": 235
          },
          {
            "workingType": "PP",
            "userId": 67,
            "infoMap": {},
            "key": 234
          },
          {
            "workingType": "PP",
            "userId": 66,
            "infoMap": {},
            "key": 233
          },
          {
            "workingType": "PP",
            "userId": 65,
            "infoMap": {},
            "key": 232
          },
          {
            "workingType": "PP",
            "userId": 62,
            "infoMap": {},
            "key": 231
          },
          {
            "workingType": "PP",
            "userId": 61,
            "infoMap": {},
            "key": 230
          },
          {
            "workingType": "PP",
            "userId": 59,
            "infoMap": {},
            "key": 229
          },
          {
            "workingType": "PP",
            "userId": 108,
            "infoMap": {},
            "key": 228
          },
          {
            "workingType": "PP",
            "userId": 116,
            "infoMap": {},
            "key": 227
          },
          {
            "workingType": "PP",
            "userId": 121,
            "infoMap": {},
            "key": 226
          },
          {
            "workingType": "PP",
            "userId": 166,
            "infoMap": {},
            "key": 243
          }
        ],
        "breaks": [
          {
            "startTime": "12:00",
            "endTime": "13:00",
            "infoMap": {},
            "key": 61
          }
        ],
        "worktimeStart": "08:30",
        "worktimeEnd": "17:30",
        "worktimeNightStart": "22:00",
        "worktimeNightEnd": "05:00",
        "vcDelegate": {
          "logger": {
            "name": "services.service.shiftworking.vc.ProjectModelViewControl"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 1,
        "companyId": 3
      },
      "startShift": "08:30",
      "endShift": "17:30",
      "startCheckin": "08:16",
      "endCheckin": "",
      "startWorking": "08:30",
      "endWorking": "17:30",
      "totalWorkTime": "08:00",
      "totalBreakTime": "01:00",
      "totalOverTime": "",
      "totalShortTime": "",
      "totalHolidayTime": "",
      "workStrategy": {},
      "vcDelegate": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "vc": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {}
    },
    {
      "userId": 121,
      "user": {
        "userAccount": "chi-dl",
        "userName": "Chi DL",
        "userNameKana": "Chi DL",
        "userMail": "chi-dl@trente.asia",
        "userType": "N",
        "adminFlag": false,
        "avatarHash": "U-dBjkWMytmy",
        "avatarPath": "/static/account/U-dBjkWMytmy?token=e680cc971f8217071d89825c8d58a76d",
        "dept": {
          "members": [],
          "managers": [],
          "cropping": {},
          "infoMap": {}
        },
        "password": "1cd7f0b9814a2c0a030bc5c7ed7521a992744b371ff607aa4dd51ec1cff3d7721646a8e5036bc0e889e46aa2ea100f7dc78edc92cd0bdf328e754aebbd42d590",
        "passcode": "2598",
        "passcodePath": "/static/passcode/3/121?token=e680cc971f8217071d89825c8d58a76d",
        "dateBirth": "1995/09/23 00:00:00",
        "joinDate": "2017/02/13 00:00:00",
        "isActivateSignin": true,
        "isActivatePassword": true,
        "services": [
          {
            "serviceCd": "TC",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_tc.png"
          },
          {
            "serviceCd": "DR",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_dr.png"
          },
          {
            "serviceCd": "MS",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_ms.png"
          },
          {
            "serviceCd": "SW",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_sw.png",
            "lastSignin": "2017/03/17 08:28:56"
          }
        ],
        "lastSignin": "2017/04/04 11:42:49",
        "vcDelegate": {
          "logger": {
            "name": "services.service.company.vc.CompanyUserModelViewController"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 121,
        "token": "b449126ae31c4db369b5858f8a606802",
        "companyId": 3
      },
      "workDate": "2017/04/04 00:00:00",
      "project": {
        "projectName": "TRENTE Vietnam",
        "projectLocation": "Hanoi Office",
        "gpsUse": false,
        "gpsMode": "OFF",
        "avatarHash": "P-xPJoWVyBlJ",
        "avatarPath": "/static/account/P-xPJoWVyBlJ?token=e680cc971f8217071d89825c8d58a76d",
        "timeUnit": "30",
        "users": [
          {
            "workingType": "PP",
            "userId": 140,
            "infoMap": {},
            "key": 242
          },
          {
            "workingType": "PP",
            "userId": 118,
            "infoMap": {},
            "key": 241
          },
          {
            "workingType": "PP",
            "userId": 114,
            "infoMap": {},
            "key": 240
          },
          {
            "workingType": "PP",
            "userId": 72,
            "infoMap": {},
            "key": 239
          },
          {
            "workingType": "PP",
            "userId": 71,
            "infoMap": {},
            "key": 238
          },
          {
            "workingType": "PP",
            "userId": 70,
            "infoMap": {},
            "key": 237
          },
          {
            "workingType": "PP",
            "userId": 69,
            "infoMap": {},
            "key": 236
          },
          {
            "workingType": "PP",
            "userId": 68,
            "infoMap": {},
            "key": 235
          },
          {
            "workingType": "PP",
            "userId": 67,
            "infoMap": {},
            "key": 234
          },
          {
            "workingType": "PP",
            "userId": 66,
            "infoMap": {},
            "key": 233
          },
          {
            "workingType": "PP",
            "userId": 65,
            "infoMap": {},
            "key": 232
          },
          {
            "workingType": "PP",
            "userId": 62,
            "infoMap": {},
            "key": 231
          },
          {
            "workingType": "PP",
            "userId": 61,
            "infoMap": {},
            "key": 230
          },
          {
            "workingType": "PP",
            "userId": 59,
            "infoMap": {},
            "key": 229
          },
          {
            "workingType": "PP",
            "userId": 108,
            "infoMap": {},
            "key": 228
          },
          {
            "workingType": "PP",
            "userId": 116,
            "infoMap": {},
            "key": 227
          },
          {
            "workingType": "PP",
            "userId": 121,
            "infoMap": {},
            "key": 226
          },
          {
            "workingType": "PP",
            "userId": 166,
            "infoMap": {},
            "key": 243
          }
        ],
        "breaks": [
          {
            "startTime": "12:00",
            "endTime": "13:00",
            "infoMap": {},
            "key": 61
          }
        ],
        "worktimeStart": "08:30",
        "worktimeEnd": "17:30",
        "worktimeNightStart": "22:00",
        "worktimeNightEnd": "05:00",
        "vcDelegate": {
          "logger": {
            "name": "services.service.shiftworking.vc.ProjectModelViewControl"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 1,
        "companyId": 3
      },
      "startShift": "08:30",
      "endShift": "17:30",
      "startCheckin": "08:16",
      "endCheckin": "",
      "startWorking": "08:30",
      "endWorking": "17:30",
      "totalWorkTime": "08:00",
      "totalBreakTime": "01:00",
      "totalOverTime": "",
      "totalShortTime": "",
      "totalHolidayTime": "",
      "workStrategy": {},
      "vcDelegate": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "vc": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {}
    },
    {
      "userId": 114,
      "user": {
        "userAccount": "doan-hq",
        "userName": "Doan HQ",
        "userNameKana": "Doan HQ",
        "userMail": "doan-hq@trente.asia",
        "userType": "N",
        "adminFlag": false,
        "avatarHash": "U-XSPbCfnOpE",
        "avatarPath": "/static/account/U-XSPbCfnOpE?token=e680cc971f8217071d89825c8d58a76d",
        "dept": {
          "members": [],
          "managers": [],
          "cropping": {},
          "infoMap": {}
        },
        "password": "3c0a9c5ea09f7b44339fab9bfd1dbe963be03680007d8340de18d80e2ec499e578ed9b892e776059ef9c7070d468ad7b0a80392ebd9bb34325ea21eae3bb7070",
        "passcode": "2080",
        "passcodePath": "/static/passcode/3/114?token=e680cc971f8217071d89825c8d58a76d",
        "dateBirth": "1993/02/01 00:00:00",
        "joinDate": "2017/01/09 00:00:00",
        "isActivateSignin": true,
        "isActivatePassword": true,
        "services": [
          {
            "serviceCd": "TC",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_tc.png"
          },
          {
            "serviceCd": "DR",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_dr.png"
          },
          {
            "serviceCd": "MS",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_ms.png"
          },
          {
            "serviceCd": "SW",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_sw.png",
            "lastSignin": "2017/04/04 15:59:35"
          }
        ],
        "lastSignin": "2017/04/04 15:59:35",
        "vcDelegate": {
          "logger": {
            "name": "services.service.company.vc.CompanyUserModelViewController"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 114,
        "token": "3433b88ca803c6ec438cd95e53f88505",
        "companyId": 3
      },
      "workDate": "2017/04/04 00:00:00",
      "project": {
        "projectName": "TRENTE Vietnam",
        "projectLocation": "Hanoi Office",
        "gpsUse": false,
        "gpsMode": "OFF",
        "avatarHash": "P-xPJoWVyBlJ",
        "avatarPath": "/static/account/P-xPJoWVyBlJ?token=e680cc971f8217071d89825c8d58a76d",
        "timeUnit": "30",
        "users": [
          {
            "workingType": "PP",
            "userId": 140,
            "infoMap": {},
            "key": 242
          },
          {
            "workingType": "PP",
            "userId": 118,
            "infoMap": {},
            "key": 241
          },
          {
            "workingType": "PP",
            "userId": 114,
            "infoMap": {},
            "key": 240
          },
          {
            "workingType": "PP",
            "userId": 72,
            "infoMap": {},
            "key": 239
          },
          {
            "workingType": "PP",
            "userId": 71,
            "infoMap": {},
            "key": 238
          },
          {
            "workingType": "PP",
            "userId": 70,
            "infoMap": {},
            "key": 237
          },
          {
            "workingType": "PP",
            "userId": 69,
            "infoMap": {},
            "key": 236
          },
          {
            "workingType": "PP",
            "userId": 68,
            "infoMap": {},
            "key": 235
          },
          {
            "workingType": "PP",
            "userId": 67,
            "infoMap": {},
            "key": 234
          },
          {
            "workingType": "PP",
            "userId": 66,
            "infoMap": {},
            "key": 233
          },
          {
            "workingType": "PP",
            "userId": 65,
            "infoMap": {},
            "key": 232
          },
          {
            "workingType": "PP",
            "userId": 62,
            "infoMap": {},
            "key": 231
          },
          {
            "workingType": "PP",
            "userId": 61,
            "infoMap": {},
            "key": 230
          },
          {
            "workingType": "PP",
            "userId": 59,
            "infoMap": {},
            "key": 229
          },
          {
            "workingType": "PP",
            "userId": 108,
            "infoMap": {},
            "key": 228
          },
          {
            "workingType": "PP",
            "userId": 116,
            "infoMap": {},
            "key": 227
          },
          {
            "workingType": "PP",
            "userId": 121,
            "infoMap": {},
            "key": 226
          },
          {
            "workingType": "PP",
            "userId": 166,
            "infoMap": {},
            "key": 243
          }
        ],
        "breaks": [
          {
            "startTime": "12:00",
            "endTime": "13:00",
            "infoMap": {},
            "key": 61
          }
        ],
        "worktimeStart": "08:30",
        "worktimeEnd": "17:30",
        "worktimeNightStart": "22:00",
        "worktimeNightEnd": "05:00",
        "vcDelegate": {
          "logger": {
            "name": "services.service.shiftworking.vc.ProjectModelViewControl"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 1,
        "companyId": 3
      },
      "startShift": "08:30",
      "endShift": "17:30",
      "startCheckin": "08:18",
      "endCheckin": "",
      "startWorking": "08:30",
      "endWorking": "17:30",
      "totalWorkTime": "08:00",
      "totalBreakTime": "01:00",
      "totalOverTime": "",
      "totalShortTime": "",
      "totalHolidayTime": "",
      "workStrategy": {},
      "vcDelegate": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "vc": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {}
    },
    {
      "userId": 65,
      "user": {
        "userAccount": "nhung-nt",
        "userName": "Nhung NT",
        "userNameKana": "Nhung NT",
        "userMail": "nhung-nt@trente.asia",
        "userType": "N",
        "adminFlag": true,
        "avatarHash": "U-bsxJoEQbCF",
        "avatarPath": "/static/account/U-bsxJoEQbCF?token=e680cc971f8217071d89825c8d58a76d",
        "dept": {
          "members": [],
          "managers": [],
          "cropping": {},
          "infoMap": {}
        },
        "password": "19f11d7740eff00febccdbfd67edab379bc5bcf251e93b030a973b8412824dda01fcbae5965e72920d583f285357c90cfb99edf533427d3906588cd6950d47d6",
        "passcode": "3234",
        "passcodePath": "/static/passcode/3/65?token=e680cc971f8217071d89825c8d58a76d",
        "dateBirth": "1986/08/27 00:00:00",
        "joinDate": "2014/07/31 00:00:00",
        "isActivateSignin": true,
        "isActivatePassword": true,
        "services": [
          {
            "serviceCd": "TC",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_tc.png"
          },
          {
            "serviceCd": "DR",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_dr.png"
          },
          {
            "serviceCd": "MS",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_ms.png"
          },
          {
            "serviceCd": "SW",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_sw.png",
            "lastSignin": "2017/04/03 13:47:23"
          }
        ],
        "lastSignin": "2017/04/04 16:16:56",
        "vcDelegate": {
          "logger": {
            "name": "services.service.company.vc.CompanyUserModelViewController"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 65,
        "token": "86c208c77e266d928512df92022ddd90",
        "companyId": 3
      },
      "workDate": "2017/04/04 00:00:00",
      "project": {
        "projectName": "TRENTE Vietnam",
        "projectLocation": "Hanoi Office",
        "gpsUse": false,
        "gpsMode": "OFF",
        "avatarHash": "P-xPJoWVyBlJ",
        "avatarPath": "/static/account/P-xPJoWVyBlJ?token=e680cc971f8217071d89825c8d58a76d",
        "timeUnit": "30",
        "users": [
          {
            "workingType": "PP",
            "userId": 140,
            "infoMap": {},
            "key": 242
          },
          {
            "workingType": "PP",
            "userId": 118,
            "infoMap": {},
            "key": 241
          },
          {
            "workingType": "PP",
            "userId": 114,
            "infoMap": {},
            "key": 240
          },
          {
            "workingType": "PP",
            "userId": 72,
            "infoMap": {},
            "key": 239
          },
          {
            "workingType": "PP",
            "userId": 71,
            "infoMap": {},
            "key": 238
          },
          {
            "workingType": "PP",
            "userId": 70,
            "infoMap": {},
            "key": 237
          },
          {
            "workingType": "PP",
            "userId": 69,
            "infoMap": {},
            "key": 236
          },
          {
            "workingType": "PP",
            "userId": 68,
            "infoMap": {},
            "key": 235
          },
          {
            "workingType": "PP",
            "userId": 67,
            "infoMap": {},
            "key": 234
          },
          {
            "workingType": "PP",
            "userId": 66,
            "infoMap": {},
            "key": 233
          },
          {
            "workingType": "PP",
            "userId": 65,
            "infoMap": {},
            "key": 232
          },
          {
            "workingType": "PP",
            "userId": 62,
            "infoMap": {},
            "key": 231
          },
          {
            "workingType": "PP",
            "userId": 61,
            "infoMap": {},
            "key": 230
          },
          {
            "workingType": "PP",
            "userId": 59,
            "infoMap": {},
            "key": 229
          },
          {
            "workingType": "PP",
            "userId": 108,
            "infoMap": {},
            "key": 228
          },
          {
            "workingType": "PP",
            "userId": 116,
            "infoMap": {},
            "key": 227
          },
          {
            "workingType": "PP",
            "userId": 121,
            "infoMap": {},
            "key": 226
          },
          {
            "workingType": "PP",
            "userId": 166,
            "infoMap": {},
            "key": 243
          }
        ],
        "breaks": [
          {
            "startTime": "12:00",
            "endTime": "13:00",
            "infoMap": {},
            "key": 61
          }
        ],
        "worktimeStart": "08:30",
        "worktimeEnd": "17:30",
        "worktimeNightStart": "22:00",
        "worktimeNightEnd": "05:00",
        "vcDelegate": {
          "logger": {
            "name": "services.service.shiftworking.vc.ProjectModelViewControl"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 1,
        "companyId": 3
      },
      "startShift": "08:30",
      "endShift": "17:30",
      "startCheckin": "08:20",
      "endCheckin": "",
      "startWorking": "08:30",
      "endWorking": "17:30",
      "totalWorkTime": "08:00",
      "totalBreakTime": "01:00",
      "totalOverTime": "",
      "totalShortTime": "",
      "totalHolidayTime": "",
      "workStrategy": {},
      "vcDelegate": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "vc": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {}
    },
    {
      "userId": 118,
      "user": {
        "userAccount": "bang-tv",
        "userName": "Bang TV",
        "userNameKana": "Bang TV",
        "userMail": "bang-tv@trente.asia",
        "userType": "N",
        "adminFlag": false,
        "avatarHash": "U-mWloSzbAeT",
        "avatarPath": "/static/account/U-mWloSzbAeT?token=e680cc971f8217071d89825c8d58a76d",
        "dept": {
          "members": [],
          "managers": [],
          "cropping": {},
          "infoMap": {}
        },
        "password": "cbd3267b14efd34613b3c04cab6c7262debebf87e52567a9c656f9725b383c0c6c788796de4a713841b554bd10afaac17c226e9a33e70d35bb3f9a83ebfe3f39",
        "passcode": "6800",
        "passcodePath": "/static/passcode/3/118?token=e680cc971f8217071d89825c8d58a76d",
        "dateBirth": "1990/02/06 00:00:00",
        "joinDate": "2017/02/06 00:00:00",
        "isActivateSignin": true,
        "isActivatePassword": true,
        "services": [
          {
            "serviceCd": "TC",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_tc.png"
          },
          {
            "serviceCd": "DR",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_dr.png"
          },
          {
            "serviceCd": "MS",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_ms.png"
          },
          {
            "serviceCd": "SW",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_sw.png",
            "lastSignin": "2017/04/03 08:28:20"
          }
        ],
        "lastSignin": "2017/04/03 08:28:20",
        "vcDelegate": {
          "logger": {
            "name": "services.service.company.vc.CompanyUserModelViewController"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 118,
        "token": "cf8813dc2521ede5d78562f2faacab8f",
        "companyId": 3
      },
      "workDate": "2017/04/04 00:00:00",
      "project": {
        "projectName": "TRENTE Vietnam",
        "projectLocation": "Hanoi Office",
        "gpsUse": false,
        "gpsMode": "OFF",
        "avatarHash": "P-xPJoWVyBlJ",
        "avatarPath": "/static/account/P-xPJoWVyBlJ?token=e680cc971f8217071d89825c8d58a76d",
        "timeUnit": "30",
        "users": [
          {
            "workingType": "PP",
            "userId": 140,
            "infoMap": {},
            "key": 242
          },
          {
            "workingType": "PP",
            "userId": 118,
            "infoMap": {},
            "key": 241
          },
          {
            "workingType": "PP",
            "userId": 114,
            "infoMap": {},
            "key": 240
          },
          {
            "workingType": "PP",
            "userId": 72,
            "infoMap": {},
            "key": 239
          },
          {
            "workingType": "PP",
            "userId": 71,
            "infoMap": {},
            "key": 238
          },
          {
            "workingType": "PP",
            "userId": 70,
            "infoMap": {},
            "key": 237
          },
          {
            "workingType": "PP",
            "userId": 69,
            "infoMap": {},
            "key": 236
          },
          {
            "workingType": "PP",
            "userId": 68,
            "infoMap": {},
            "key": 235
          },
          {
            "workingType": "PP",
            "userId": 67,
            "infoMap": {},
            "key": 234
          },
          {
            "workingType": "PP",
            "userId": 66,
            "infoMap": {},
            "key": 233
          },
          {
            "workingType": "PP",
            "userId": 65,
            "infoMap": {},
            "key": 232
          },
          {
            "workingType": "PP",
            "userId": 62,
            "infoMap": {},
            "key": 231
          },
          {
            "workingType": "PP",
            "userId": 61,
            "infoMap": {},
            "key": 230
          },
          {
            "workingType": "PP",
            "userId": 59,
            "infoMap": {},
            "key": 229
          },
          {
            "workingType": "PP",
            "userId": 108,
            "infoMap": {},
            "key": 228
          },
          {
            "workingType": "PP",
            "userId": 116,
            "infoMap": {},
            "key": 227
          },
          {
            "workingType": "PP",
            "userId": 121,
            "infoMap": {},
            "key": 226
          },
          {
            "workingType": "PP",
            "userId": 166,
            "infoMap": {},
            "key": 243
          }
        ],
        "breaks": [
          {
            "startTime": "12:00",
            "endTime": "13:00",
            "infoMap": {},
            "key": 61
          }
        ],
        "worktimeStart": "08:30",
        "worktimeEnd": "17:30",
        "worktimeNightStart": "22:00",
        "worktimeNightEnd": "05:00",
        "vcDelegate": {
          "logger": {
            "name": "services.service.shiftworking.vc.ProjectModelViewControl"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 1,
        "companyId": 3
      },
      "startShift": "08:30",
      "endShift": "17:30",
      "startCheckin": "08:25",
      "endCheckin": "",
      "startWorking": "08:30",
      "endWorking": "17:30",
      "totalWorkTime": "08:00",
      "totalBreakTime": "01:00",
      "totalOverTime": "",
      "totalShortTime": "",
      "totalHolidayTime": "",
      "workStrategy": {},
      "vcDelegate": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "vc": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {}
    },
    {
      "userId": 166,
      "user": {
        "userAccount": "tien-lt",
        "userName": "Tien LT",
        "userNameKana": "Tien LT",
        "userMail": "tien-lt@trente.asia",
        "userType": "N",
        "adminFlag": false,
        "avatarHash": "U-QpghfOksaL",
        "avatarPath": "/static/account/U-QpghfOksaL?token=e680cc971f8217071d89825c8d58a76d",
        "dept": {
          "members": [],
          "managers": [],
          "cropping": {},
          "infoMap": {}
        },
        "password": "a831e3fa5d00e0c6ba3c06186c8cc2dadd06baf019454f90dc4d494bb3eed32300afffce8e92a5e82677127833b0bd4904e5caff1a3fc9a0e5ae573308d9a1d1",
        "passcode": "6224",
        "passcodePath": "/static/passcode/3/166?token=e680cc971f8217071d89825c8d58a76d",
        "dateBirth": "1990/12/02 00:00:00",
        "joinDate": "2017/04/03 00:00:00",
        "isActivateSignin": true,
        "isActivatePassword": true,
        "services": [
          {
            "serviceCd": "TC",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_tc.png"
          },
          {
            "serviceCd": "DR",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_dr.png"
          },
          {
            "serviceCd": "MS",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_ms.png"
          },
          {
            "serviceCd": "SW",
            "pushSetting": true,
            "serviceIcon": "images/service/icon_sw.png"
          }
        ],
        "lastSignin": "2017/04/04 14:52:15",
        "vcDelegate": {
          "logger": {
            "name": "services.service.company.vc.CompanyUserModelViewController"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 166,
        "token": "79f8bc1128570c5d6d6cda2a66541faa",
        "companyId": 3
      },
      "workDate": "2017/04/04 00:00:00",
      "project": {
        "projectName": "TRENTE Vietnam",
        "projectLocation": "Hanoi Office",
        "gpsUse": false,
        "gpsMode": "OFF",
        "avatarHash": "P-xPJoWVyBlJ",
        "avatarPath": "/static/account/P-xPJoWVyBlJ?token=e680cc971f8217071d89825c8d58a76d",
        "timeUnit": "30",
        "users": [
          {
            "workingType": "PP",
            "userId": 140,
            "infoMap": {},
            "key": 242
          },
          {
            "workingType": "PP",
            "userId": 118,
            "infoMap": {},
            "key": 241
          },
          {
            "workingType": "PP",
            "userId": 114,
            "infoMap": {},
            "key": 240
          },
          {
            "workingType": "PP",
            "userId": 72,
            "infoMap": {},
            "key": 239
          },
          {
            "workingType": "PP",
            "userId": 71,
            "infoMap": {},
            "key": 238
          },
          {
            "workingType": "PP",
            "userId": 70,
            "infoMap": {},
            "key": 237
          },
          {
            "workingType": "PP",
            "userId": 69,
            "infoMap": {},
            "key": 236
          },
          {
            "workingType": "PP",
            "userId": 68,
            "infoMap": {},
            "key": 235
          },
          {
            "workingType": "PP",
            "userId": 67,
            "infoMap": {},
            "key": 234
          },
          {
            "workingType": "PP",
            "userId": 66,
            "infoMap": {},
            "key": 233
          },
          {
            "workingType": "PP",
            "userId": 65,
            "infoMap": {},
            "key": 232
          },
          {
            "workingType": "PP",
            "userId": 62,
            "infoMap": {},
            "key": 231
          },
          {
            "workingType": "PP",
            "userId": 61,
            "infoMap": {},
            "key": 230
          },
          {
            "workingType": "PP",
            "userId": 59,
            "infoMap": {},
            "key": 229
          },
          {
            "workingType": "PP",
            "userId": 108,
            "infoMap": {},
            "key": 228
          },
          {
            "workingType": "PP",
            "userId": 116,
            "infoMap": {},
            "key": 227
          },
          {
            "workingType": "PP",
            "userId": 121,
            "infoMap": {},
            "key": 226
          },
          {
            "workingType": "PP",
            "userId": 166,
            "infoMap": {},
            "key": 243
          }
        ],
        "breaks": [
          {
            "startTime": "12:00",
            "endTime": "13:00",
            "infoMap": {},
            "key": 61
          }
        ],
        "worktimeStart": "08:30",
        "worktimeEnd": "17:30",
        "worktimeNightStart": "22:00",
        "worktimeNightEnd": "05:00",
        "vcDelegate": {
          "logger": {
            "name": "services.service.shiftworking.vc.ProjectModelViewControl"
          },
          "loginUserId": 72,
          "messageList": [],
          "map": {}
        },
        "infoMap": {},
        "key": 1,
        "companyId": 3
      },
      "startShift": "08:30",
      "endShift": "17:30",
      "startCheckin": "08:25",
      "endCheckin": "",
      "startWorking": "08:30",
      "endWorking": "17:30",
      "totalWorkTime": "08:00",
      "totalBreakTime": "01:00",
      "totalOverTime": "",
      "totalShortTime": "",
      "totalHolidayTime": "",
      "workStrategy": {},
      "vcDelegate": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "vc": {
        "logger": {
          "name": "asia.chiase.play25.view.ViewControlCommon"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {}
    }
  ],
  "workSummary": {
    "regularDays": 0,
    "workingDays": 0,
    "countPaidVacation": 0,
    "countSpcialVacation": 0,
    "countCompVacation": 0,
    "countAbsent": 0,
    "countHolidayWork": 0,
    "countBeLate": 0,
    "listBeLate": [],
    "countLeaveEarly": 0,
    "listLeaveEarly": [],
    "infoMap": {}
  },
  "lines": [],
  "uncheckedCount": 0,
  "noticeCount": 0,
  "transitCount": 0,
  "status": "OK",
  "hasReturnCode": false,
  "returnCode": "0",
  "myself": {
    "userAccount": "viet-nh",
    "userName": "Viet NH",
    "userNameKana": "Viet NH",
    "userMail": "viet-nh@trente.asia",
    "userType": "N",
    "adminFlag": false,
    "avatarHash": "U-TBxIbKXubL",
    "avatarPath": "/static/account/U-TBxIbKXubL?token=e680cc971f8217071d89825c8d58a76d",
    "dept": {
      "deptName": "TRENTE",
      "avatarHash": "D-RmDnuUmrBt",
      "avatarPath": "/static/account/D-RmDnuUmrBt?token=e680cc971f8217071d89825c8d58a76d",
      "members": [
        {
          "userAccount": "takano-yasuhiro",
          "userName": "高野　康弘",
          "userNameKana": "Yasuhiro Takano",
          "userMail": "takano-yasuhiro@trente.asia",
          "userType": "N",
          "adminFlag": true,
          "avatarHash": "U-uYtzsjyqrc",
          "avatarPath": "/static/account/U-uYtzsjyqrc?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "73dc24c642c00524eaf318ba1d3b85c22863aab3b070f97d075a0f5509d833e4f8fb9e8793791b35b8bea704a9861957490c8f054428f4648203b9523815294b",
          "passcode": "4094",
          "passcodePath": "/static/passcode/3/59?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1977/12/25 00:00:00",
          "joinDate": "2014/07/31 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png",
              "lastSignin": "2017/03/04 10:34:49"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png",
              "lastSignin": "2016/12/06 15:36:48"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png",
              "lastSignin": "2016/12/07 17:17:02"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png",
              "lastSignin": "2017/04/04 17:22:09"
            },
            {
              "serviceCd": "CL",
              "pushSetting": true,
              "lastSignin": "2017/04/04 14:57:32"
            }
          ],
          "lastSignin": "2017/04/04 17:22:09",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 59,
          "token": "d82a3e01fb9babea4d627f3b04194665",
          "companyId": 3
        },
        {
          "userAccount": "lien-nt",
          "userName": "Lien NT",
          "userNameKana": "Lien NT",
          "userMail": "lien-nt@trente.asia",
          "userType": "N",
          "adminFlag": false,
          "avatarPath": "/assets/images/default/default_avatar2.png",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "a9085c66e31a9c27822c495e8e2a62b956032b82b9486e4f68276fadd0b10f8885671f1d830039cde75f479cb5467de37a80e28fe34ee785a3ede75ebfc6d42e",
          "passcode": "4591",
          "passcodePath": "/static/passcode/3/64?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1990/06/27 00:00:00",
          "joinDate": "2014/11/03 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            }
          ],
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 64,
          "token": "a434f740ea0d53e406593de3d677f7ad",
          "companyId": 3
        },
        {
          "userAccount": "nhung-nt",
          "userName": "Nhung NT",
          "userNameKana": "Nhung NT",
          "userMail": "nhung-nt@trente.asia",
          "userType": "N",
          "adminFlag": true,
          "avatarHash": "U-bsxJoEQbCF",
          "avatarPath": "/static/account/U-bsxJoEQbCF?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "19f11d7740eff00febccdbfd67edab379bc5bcf251e93b030a973b8412824dda01fcbae5965e72920d583f285357c90cfb99edf533427d3906588cd6950d47d6",
          "passcode": "3234",
          "passcodePath": "/static/passcode/3/65?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1986/08/27 00:00:00",
          "joinDate": "2014/07/31 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png",
              "lastSignin": "2017/04/03 13:47:23"
            }
          ],
          "lastSignin": "2017/04/04 16:16:56",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 65,
          "token": "86c208c77e266d928512df92022ddd90",
          "companyId": 3
        },
        {
          "userAccount": "phuong-lt",
          "userName": "Phuong LT",
          "userNameKana": "Phuong LT",
          "userMail": "phuong-lt@trente.asia",
          "userType": "N",
          "adminFlag": false,
          "avatarHash": "U-ZoaxsKsohM",
          "avatarPath": "/static/account/U-ZoaxsKsohM?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "04a73c9ae9fd71f003d4a2928da170223711b916e32369c995af9629cdf340eaf1b198d71faff93165fa40d374711ee44685bc8eb31e2cb0eb162f560c9ac11b",
          "passcode": "2689",
          "passcodePath": "/static/passcode/3/66?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1992/10/19 00:00:00",
          "joinDate": "2015/06/01 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png",
              "lastSignin": "2017/02/27 08:23:37"
            }
          ],
          "lastSignin": "2017/03/23 11:25:56",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 66,
          "token": "3df51d19016769e159415ade99982d50",
          "companyId": 3
        },
        {
          "userAccount": "phuong-dt",
          "userName": "Phuong DT",
          "userNameKana": "Phuong DT",
          "userMail": "phuong-dt@trente.asia",
          "userType": "N",
          "adminFlag": false,
          "avatarHash": "U-ivfjCFyMqv",
          "avatarPath": "/static/account/U-ivfjCFyMqv?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "a0b71da90a22915d79395a59a7432d96f877c124eccdf7556d74ee756712a597b52ab545a1a959bf02aa6cf298f8f87dbe82a7fb338c656d8c96bb33d8effa34",
          "passcode": "1942",
          "passcodePath": "/static/passcode/3/67?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1987/09/24 00:00:00",
          "joinDate": "2015/07/01 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png",
              "lastSignin": "2017/02/18 08:59:35"
            }
          ],
          "lastSignin": "2017/03/03 19:34:03",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 67,
          "token": "921fd6994d20c5cd07c2552a386047b0",
          "companyId": 3
        },
        {
          "userAccount": "thuy-tt",
          "userName": "Thuy TT",
          "userNameKana": "Thuy TT",
          "userMail": "thuy-tt@trente.asia",
          "userType": "N",
          "adminFlag": false,
          "avatarHash": "U-jYEBAZxKFM",
          "avatarPath": "/static/account/U-jYEBAZxKFM?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "4eb9f875602f5245b12f18d7b6ba47d6f83fca17119c16d7e3e4106495205452b28c71f25e377f236a42779bc0a231a021096b7059726595decd8c3a9675df2a",
          "passcode": "9631",
          "passcodePath": "/static/passcode/3/69?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1992/09/02 00:00:00",
          "joinDate": "2015/06/01 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png",
              "lastSignin": "2017/04/03 19:19:29"
            }
          ],
          "lastSignin": "2017/04/04 09:56:22",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 69,
          "token": "1e4ab99108ebaee9cfbaa20d091da57b",
          "companyId": 3
        },
        {
          "userAccount": "trung-nd",
          "userName": "Trung ND",
          "userNameKana": "Trung ND",
          "userMail": "trung-nd@trente.asia",
          "userType": "N",
          "adminFlag": false,
          "avatarHash": "U-RTuiVvimqu",
          "avatarPath": "/static/account/U-RTuiVvimqu?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "f5ed8b1a702380cf6feeb94aa7878596ffe612bb71fddd7ad1c0064621a6f8cd82d4c43aceed522b545c1061df407e04bea37ff2769f656e662618c679d4e47a",
          "passcode": "3469",
          "passcodePath": "/static/passcode/3/70?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1987/10/30 00:00:00",
          "joinDate": "2014/07/31 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png",
              "lastSignin": "2017/03/01 17:21:33"
            }
          ],
          "lastSignin": "2017/03/27 11:16:32",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 70,
          "token": "e64ad975c7e5004b17551c523074986e",
          "companyId": 3
        },
        {
          "userAccount": "viet-nh",
          "userName": "Viet NH",
          "userNameKana": "Viet NH",
          "userMail": "viet-nh@trente.asia",
          "userType": "N",
          "adminFlag": false,
          "avatarHash": "U-TBxIbKXubL",
          "avatarPath": "/static/account/U-TBxIbKXubL?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "2935abac79b93fd5f76ad791834f6af71572daa2d68d8ea43c472e7525540b889896a159f030b49e0d2321d7c07b282a6df7f9c3dd4850bf26b8779cbde4b40c",
          "passcode": "4321",
          "passcodePath": "/static/passcode/3/72?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1990/03/12 00:00:00",
          "joinDate": "2015/06/22 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png",
              "lastSignin": "2017/04/04 15:51:56"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png",
              "lastSignin": "2017/04/04 17:22:10"
            }
          ],
          "lastSignin": "2017/04/04 17:22:10",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 72,
          "token": "904c0369f607677e6dab5eb531a675d4",
          "companyId": 3
        },
        {
          "userAccount": "doan-hq",
          "userName": "Doan HQ",
          "userNameKana": "Doan HQ",
          "userMail": "doan-hq@trente.asia",
          "userType": "N",
          "adminFlag": false,
          "avatarHash": "U-XSPbCfnOpE",
          "avatarPath": "/static/account/U-XSPbCfnOpE?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "3c0a9c5ea09f7b44339fab9bfd1dbe963be03680007d8340de18d80e2ec499e578ed9b892e776059ef9c7070d468ad7b0a80392ebd9bb34325ea21eae3bb7070",
          "passcode": "2080",
          "passcodePath": "/static/passcode/3/114?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1993/02/01 00:00:00",
          "joinDate": "2017/01/09 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png",
              "lastSignin": "2017/04/04 15:59:35"
            }
          ],
          "lastSignin": "2017/04/04 15:59:35",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 114,
          "token": "3433b88ca803c6ec438cd95e53f88505",
          "companyId": 3
        },
        {
          "userAccount": "hoan-ht",
          "userName": "Hoan HT",
          "userNameKana": "Hoan HT",
          "userMail": "hoan-ht@trente.asia",
          "userType": "N",
          "adminFlag": false,
          "avatarHash": "U-XGiRpWNWCA",
          "avatarPath": "/static/account/U-XGiRpWNWCA?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "3ec32fb8220d7aa1bc192a5b8cdc198834548e1893e36526b0f2eccac73a94bff357fc8efc24d4bc555fbaa88da684b53a9b5e9b17828bee38744027066ca154",
          "passcode": "3831",
          "passcodePath": "/static/passcode/3/116?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1994/07/20 00:00:00",
          "joinDate": "2017/01/16 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png",
              "lastSignin": "2017/03/25 21:55:23"
            }
          ],
          "lastSignin": "2017/04/04 09:37:47",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 116,
          "token": "eb46d55bd03003d68895dc90976f35be",
          "companyId": 3
        },
        {
          "userAccount": "chi-dl",
          "userName": "Chi DL",
          "userNameKana": "Chi DL",
          "userMail": "chi-dl@trente.asia",
          "userType": "N",
          "adminFlag": false,
          "avatarHash": "U-dBjkWMytmy",
          "avatarPath": "/static/account/U-dBjkWMytmy?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "1cd7f0b9814a2c0a030bc5c7ed7521a992744b371ff607aa4dd51ec1cff3d7721646a8e5036bc0e889e46aa2ea100f7dc78edc92cd0bdf328e754aebbd42d590",
          "passcode": "2598",
          "passcodePath": "/static/passcode/3/121?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1995/09/23 00:00:00",
          "joinDate": "2017/02/13 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png",
              "lastSignin": "2017/03/17 08:28:56"
            }
          ],
          "lastSignin": "2017/04/04 11:42:49",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 121,
          "token": "b449126ae31c4db369b5858f8a606802",
          "companyId": 3
        },
        {
          "userAccount": "bang-tv",
          "userName": "Bang TV",
          "userNameKana": "Bang TV",
          "userMail": "bang-tv@trente.asia",
          "userType": "N",
          "adminFlag": false,
          "avatarHash": "U-mWloSzbAeT",
          "avatarPath": "/static/account/U-mWloSzbAeT?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "cbd3267b14efd34613b3c04cab6c7262debebf87e52567a9c656f9725b383c0c6c788796de4a713841b554bd10afaac17c226e9a33e70d35bb3f9a83ebfe3f39",
          "passcode": "6800",
          "passcodePath": "/static/passcode/3/118?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1990/02/06 00:00:00",
          "joinDate": "2017/02/06 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png",
              "lastSignin": "2017/04/03 08:28:20"
            }
          ],
          "lastSignin": "2017/04/03 08:28:20",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 118,
          "token": "cf8813dc2521ede5d78562f2faacab8f",
          "companyId": 3
        },
        {
          "userAccount": "tanaka-takayuki",
          "userName": "Tanaka Takayuki",
          "userNameKana": "Tanaka Takayuki",
          "userMail": "tanaka-takayuki@trente.asia",
          "userType": "N",
          "adminFlag": true,
          "avatarHash": "U-aTVRmxruXm",
          "avatarPath": "/static/account/U-aTVRmxruXm?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "7ba3365acaafbb26f2562fad8f54687084e2b734c672ef690a1a6030d04eb10594e98e952cd0f38b4c1e34dadb8463ee49077d1b53e2245fa3268ee72c4ca209",
          "passcode": "0096",
          "passcodePath": "/static/passcode/3/140?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1978/12/17 00:00:00",
          "joinDate": "2017/03/01 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png"
            }
          ],
          "lastSignin": "2017/04/04 10:38:25",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 140,
          "token": "7fa6d4fb0b4fffdb567edcb80a1b57a1",
          "companyId": 3
        },
        {
          "userAccount": "tien-lt",
          "userName": "Tien LT",
          "userNameKana": "Tien LT",
          "userMail": "tien-lt@trente.asia",
          "userType": "N",
          "adminFlag": false,
          "avatarHash": "U-QpghfOksaL",
          "avatarPath": "/static/account/U-QpghfOksaL?token=e680cc971f8217071d89825c8d58a76d",
          "dept": {
            "members": [],
            "managers": [],
            "cropping": {},
            "infoMap": {}
          },
          "deptBoardId": 30,
          "password": "a831e3fa5d00e0c6ba3c06186c8cc2dadd06baf019454f90dc4d494bb3eed32300afffce8e92a5e82677127833b0bd4904e5caff1a3fc9a0e5ae573308d9a1d1",
          "passcode": "6224",
          "passcodePath": "/static/passcode/3/166?token=e680cc971f8217071d89825c8d58a76d",
          "dateBirth": "1990/12/02 00:00:00",
          "joinDate": "2017/04/03 00:00:00",
          "isActivateSignin": true,
          "isActivatePassword": true,
          "services": [
            {
              "serviceCd": "TC",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_tc.png"
            },
            {
              "serviceCd": "DR",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_dr.png"
            },
            {
              "serviceCd": "MS",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_ms.png"
            },
            {
              "serviceCd": "SW",
              "pushSetting": true,
              "serviceIcon": "images/service/icon_sw.png"
            }
          ],
          "lastSignin": "2017/04/04 14:52:15",
          "vcDelegate": {
            "logger": {
              "name": "services.service.company.vc.CompanyUserModelViewController"
            },
            "loginUserId": 72,
            "messageList": [],
            "map": {}
          },
          "infoMap": {},
          "key": 166,
          "token": "79f8bc1128570c5d6d6cda2a66541faa",
          "companyId": 3
        }
      ],
      "managers": [],
      "cropping": {},
      "boardNote": "",
      "vcDelegate": {
        "logger": {
          "name": "services.service.company.vc.CompanyDeptModelViewController"
        },
        "loginUserId": 72,
        "messageList": [],
        "map": {}
      },
      "infoMap": {},
      "key": 8
    },
    "deptBoardId": 30,
    "password": "2935abac79b93fd5f76ad791834f6af71572daa2d68d8ea43c472e7525540b889896a159f030b49e0d2321d7c07b282a6df7f9c3dd4850bf26b8779cbde4b40c",
    "passcode": "4321",
    "passcodePath": "/static/passcode/3/72?token=e680cc971f8217071d89825c8d58a76d",
    "dateBirth": "1990/03/12 00:00:00",
    "joinDate": "2015/06/22 00:00:00",
    "isActivateSignin": true,
    "isActivatePassword": true,
    "services": [
      {
        "serviceCd": "TC",
        "pushSetting": true,
        "serviceIcon": "images/service/icon_tc.png"
      },
      {
        "serviceCd": "DR",
        "pushSetting": true,
        "serviceIcon": "images/service/icon_dr.png",
        "lastSignin": "2017/04/04 15:51:56"
      },
      {
        "serviceCd": "MS",
        "pushSetting": true,
        "serviceIcon": "images/service/icon_ms.png"
      },
      {
        "serviceCd": "SW",
        "pushSetting": true,
        "serviceIcon": "images/service/icon_sw.png",
        "lastSignin": "2017/04/04 17:22:10"
      }
    ],
    "lastSignin": "2017/04/04 17:22:10",
    "vcDelegate": {
      "logger": {
        "name": "services.service.company.vc.CompanyUserModelViewController"
      },
      "loginUserId": 72,
      "messageList": [],
      "map": {}
    },
    "infoMap": {},
    "key": 72,
    "token": "904c0369f607677e6dab5eb531a675d4",
    "companyId": 3
  },
  "setting": {
    "DR_PUBLIC_LEVEL": "DEPT",
    "CP_FISCAL_MONTH": "SM04",
    "MS_AI_USER_NAME": "Fukuri AI",
    "CP_LIKE_MAX": "5",
    "KEY_SW_PUSH_BEFORE": "5",
    "KEY_SW_PUSH_AFTER": "30",
    "KEY_SW_PUSH_OVERTIME": "60",
    "KEY_SW_VIEW_HOUR_START": "07:00",
    "KEY_SW_VIEW_HOUR_END": "19:00",
    "SW_APPOINTMENT_ENABLED": "1",
    "DR_CUSTOM_FIELD": "How was today?",
    "REGULAR_HOLIDAY": "1,7,0",
    "KEY_SW_WORKTIME_CALC_TYPE": "OFR",
    "KEY_CL_START_DAY_OF_WEEK": "1",
    "KEY_CL_DAILY_REMINDER": "08:00",
    "KEY_CL_SHOW_HOLIDAY": "false",
    "KEY_CL_SHOW_WORK_OFFER": "false",
    "KEY_CL_SHOW_BIRTHDAY": "false",
    "WF_MAX_FILE_SIZE": "52428800",
    "WF_PUSH_SETTING": "1"
  },
  "goals": []
}